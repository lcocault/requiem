# Scope

The RequieM management tool is a set of features to manage and trace coverage
of requirements for a Java project.

#License

It is licensed by Laurent COCAULT under the Apache License Version 2.0. A copy
of this license is provided in the LICENSE.txt file.

# Content

The src/main/java directory contains the library sources.
The src/main/resources directory contains the library data.
The src/test/java directory contains the tests sources.
The src/test/resources directory contains the tests data.

# Dependencies

RequieM relies on the following free software, all released under business
friendly free licenses.

permanent dependency:

  - None

test-time dependency:

  - JUnit 5.4.2 from Erich Gamma and Kent Beck
    http://www.junit.org/
    released under the Common Public License Version 1.0

# Usage

## Requirement specification

The requirements shall be specified in an XML file that may include several
versions of the same requirement and its applicability status. An exemple of
requirement specification is provided hereafter:

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <!-- Specifications of the REST-O system.
      -->
    <xmlRequirementRegistry>
        <requirements>
            <identifier>RESTO-CORE-REST-F-010</identifier>
            <type>FUNCTIONAL</type>
            <versions>
                <state>APPROVED</state>
                <text>A restaurant is characterized by a unique name</text>
                <version>1</version>
            </versions>
        </requirements>
        <requirements>
            <identifier>RESTO-CORE-REST-F-020</identifier>
            <type>FUNCTIONAL</type>
            <versions>
                <state>APPROVED</state>
                <text>A restaurant is composed of one to several areas</text>
                <version>1</version>
            </versions>
        </requirements>
    </xmlRequirementRegistry>

This file may be stored with the resources of the project (typically in the
main/resources of a Maven module). 

## Unit test coverage

To check the coverage of requirements by unit tests requires modifications in
both the Java unit test code and the project POM.

### Java code sample using the historical API (for JUnit4 users)

The Java unit code must:

* Declare an instance of the CoverageReport class

* Initialize the coverage report, using the requirement specification (XML)

* Save the coverage report after the tests

* Add assertions of coverage

Examples of these steps are provided hereafter.

    protected static CoverageReport report_;
    ...
    @BeforeClass
    public static void initCoverageAnalysis() throws IOException {
        try {
            RequirementRegistryFactory regFactory = new RequirementRegistryFactory();
            CoverageReportFactory repFactory = new CoverageReportFactory();
            registry_ = regFactory.createRegistry();
            report_ = repFactory.createReport();
            registry_.load();
            report_.initialize(registry_);
            report_.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @AfterClass
    public static void finalizeCoverageAnalysis() throws IOException {
        report_.save();
    }
    ...
    report_.add("RESTO-CORE-REST-F-010 v1", CoverageStatus.COVERED);
    ...
    report_.add("RESTO-CORE-REST-F-010 v1", CoverageStatus.NOT_COVERED);
    

### Java code sample using the annotation (for JUnit5 users)

The Java unit code must:

* Annotate the unit test class with the CoverageUpdater extension

* Initialize the coverage report, using the requirement specification (XML)

* Save the coverage report after the tests

* Annotate the unit tests with @Cover

Examples of these steps are provided hereafter.

    protected static CoverageReport report_;
    ...
    @BeforeClass
    public static void initCoverageAnalysis() throws IOException {
        try {
            RequirementRegistryFactory regFactory = new RequirementRegistryFactory();
            CoverageReportFactory repFactory = new CoverageReportFactory();
            registry_ = regFactory.createRegistry();
            report_ = repFactory.createReport();
            registry_.load();
            report_.initialize(registry_);
            report_.load();
            CoverageUpdater.setReport(report_);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @AfterClass
    public static void finalizeCoverageAnalysis() throws IOException {
        report_.save();
    }
    ...
    @Test
    @Cover(requirements = {""RESTO-CORE-REST-F-010 v1"})
    public void myTest() {
    }    

### POM sample

The POM of the module must be modified as follow:

    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${resto-maven-surefire}</version>
        <configuration>
            <systemPropertyVariables>
                <fr.tools.requiem.xml.registryPath>${project.build.outputDirectory}/specifications.xml</fr.tools.requiem.xml.registryPath>
                <fr.tools.requiem.xml.reportPath>${project.build.outputDirectory}/coverage.xml</fr.tools.requiem.xml.reportPath>
            </systemPropertyVariables>
        </configuration>
    </plugin>

The value of the system property variables must be adapted depending on the
module organization. Note that an empty coverage file must be provided as an
input.

## HTML report generation

Once the XML coverage is generated, it is possible to render it as an HTML
report thanks to an XSL style sheet. An example of such a style sheet is
provided with the RequieM module. To generate the HTML report, the RequieM
Maven plugin must be included in your project POM as follow:

    <plugin>
        <groupId>fr.tools</groupId>
        <artifactId>requiem</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <executions>
            <execution>
                <phase>package</phase>
                <configuration>
                    <xmlReport>${project.build.outputDirectory}/coverage.xml</xmlReport>
                    <htmlReport>${project.build.outputDirectory}/coverage.html</htmlReport>
                    <stylesheet>${project.build.outputDirectory}/htmlreport.xsl</stylesheet>
                </configuration>
                <goals>
                    <goal>requiem</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

Once again, the values of the variables must be adapted depending on the
project organization.
