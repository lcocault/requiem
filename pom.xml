<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>fr.tools</groupId>
	<artifactId>requiem</artifactId>
	<version>0.0.2-SNAPSHOT</version>
	<packaging>maven-plugin</packaging>
	<name>RequieM</name>
	<description>Requirement management system</description>
	<url>TBD</url>

	<!-- License -->
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>

	<!-- Development team -->
	<developers>
		<developer>
			<name>Laurent COCAULT</name>
			<email>laurent_cocault@yahoo.fr</email>
			<roles>
				<role>architect</role>
				<role>developer</role>
			</roles>
		</developer>
	</developers>

	<!-- Properties -->
	<properties>

		<!-- General properties -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<!-- Plugin versions -->
		<requiem-maven-compiler>3.2</requiem-maven-compiler>
		<requiem-maven-source>2.4</requiem-maven-source>
		<requiem-maven-resources>2.7</requiem-maven-resources>
		<requiem-maven-jacoco>0.8.4</requiem-maven-jacoco>
		<requiem-maven-project-report>2.7</requiem-maven-project-report>
		<requiem-maven-checkstyle>2.13</requiem-maven-checkstyle>

	</properties>

	<!-- Build for Java8 with sources -->
	<build>
		<plugins>

			<!-- Compiler -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${requiem-maven-compiler}</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<!-- Attach sources -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>${requiem-maven-source}</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Copy the resources -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>${requiem-maven-resources}</version>
				<executions>
					<execution>
						<phase>process-resources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.outputDirectory}/META-INF</outputDirectory>
							<resources>
								<resource>
									<directory>.</directory>
									<includes>
										<include>LICENSE.txt</include>
										<include>NOTICE.txt</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Code coverage -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${requiem-maven-jacoco}</version>
				<executions>
					<execution>
						<id>prepare-agent</id>
						<phase>process-test-classes</phase>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<phase>test</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-plugin-plugin</artifactId>
				<version>3.6.0</version>
				<configuration>
					<goalPrefix>plugin</goalPrefix>
					<outputDirectory>target/dir</outputDirectory>
				</configuration>
			</plugin>

		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.jacoco</groupId>
										<artifactId>
											jacoco-maven-plugin
										</artifactId>
										<versionRange>
											[0.7.4.201502262128,)
										</versionRange>
										<goals>
											<goal>prepare-agent</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<reporting>
		<plugins>

			<!-- Project info report -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>${requiem-maven-project-report}</version>
			</plugin>

			<!-- Code is checked with checkstyle -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>${requiem-maven-checkstyle}</version>
				<configuration>
					<configLocation>${basedir}/checkstyle.xml</configLocation>
					<enableRulesSummary>false</enableRulesSummary>
					<headerLocation>${basedir}/license-header.txt</headerLocation>
					<resourceExcludes>*.properties</resourceExcludes>
				</configuration>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>

			<!-- Code coverage is checked with Jacoco -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${requiem-maven-jacoco}</version>
			</plugin>

		</plugins>
	</reporting>

	<dependencies>

		<!-- Standard technologies -->
		<dependency>
			<groupId>javax.xml</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.1</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>5.4.2</version>
		</dependency>

		<!-- Maven plugin dependencies -->
		<dependency>
			<groupId>org.apache.maven</groupId>
			<artifactId>maven-plugin-api</artifactId>
			<version>3.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.maven.plugin-tools</groupId>
			<artifactId>maven-plugin-annotations</artifactId>
			<version>3.4</version>
			<scope>provided</scope>
		</dependency>

		<!-- Test dependencies -->
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>2.3.2</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.sun.istack</groupId>
			<artifactId>istack-commons-runtime</artifactId>
			<version>3.0.8</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

</project>
