/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.tools.requiem.coverage.CoverageReport;
import fr.tools.requiem.coverage.CoverageStatus;
import fr.tools.requiem.coverage.RequirementCoverage;
import fr.tools.requiem.requirement.Requirement;
import fr.tools.requiem.requirement.RequirementRegistry;
import fr.tools.requiem.requirement.RequirementState;
import fr.tools.requiem.requirement.RequirementVersion;

/**
 * Coverage report.
 */
@XmlRootElement
public class XmlCoverageReport implements CoverageReport {

    /** System property to access the report file. */
    public static final String REPORT_FILE = "fr.tools.requiem.xml.reportPath";

    /** Format of the key. */
    private static final String KEY_FORMAT = "%s %s";

    /** Logger for warnings. */
    private Logger logger_;

    /** Coverage indexed by requirement. */
    private Map<String, CoverageSet> coverage_;

    /**
     * Requirements coverage.
     */
    public XmlCoverageReport() {

        // Initialize the collections of the report
        coverage_ = new HashMap<String, CoverageSet>();
        logger_ = Logger.getLogger(getClass().getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(final String requirement, final String version,
            final CoverageStatus status) {

        // Get the set of requirements
        final String key = String.format(KEY_FORMAT, requirement, version);
        final CoverageSet set = coverage_.get(key);

        // When the set exists, add the given coverage. Otherwise, warn that a
        // covered requirement is no longer applicable.
        if (set == null) {

            // Warn that the requirement version is unknown
            logger_.warning("Unknown requirement " + key);

        } else {

            // Add the requirement coverage
            set.add(new RequirementCoverage(key, status));
        }
    }

    /**
     * Get the requirement coverage.
     * @return Coverage of requirements
     */
    public Map<String, CoverageSet> getCoverage() {
        // The value returned is a newly created map
        final Map<String, CoverageSet> result = new HashMap<String, CoverageSet>();
        result.putAll(coverage_);
        return coverage_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CoverageStatus getStatus(final String requirement) {
        if (coverage_.get(requirement) == null) {
            // Requirement not managed
            return null;
        } else {
            // Aggregated status for the requirement
            return coverage_.get(requirement).getStatus();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CoverageStatus getStatus(final String requirement,
            final String version) {
        // TODO Manage the version of the requirement
        throw new RuntimeException("Not implemented yet");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, CoverageStatus> getStatuses() {
        // The value returned is a newly created map
        final Map<String, CoverageStatus> result = new HashMap<String, CoverageStatus>();
        // The content of the map is a synthesis of the coverage details
        for (String requirement : coverage_.keySet()) {
            result.put(requirement, coverage_.get(requirement).getStatus());
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final RequirementRegistry registry) {

        // Map the all the requirement version with their requirement, including
        // not applicable requirement versions.
        for (Requirement requirement : registry.getRequirements()) {
            for (RequirementVersion version : requirement.getVersions()) {
                if (version.getState() == RequirementState.APPROVED) {
                    coverage_.put(version.toString(), new CoverageSet());
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() throws IOException {
        // Attempt to load an existing version of the report
        final String reportFile = System.getProperty(REPORT_FILE);
        if (reportFile == null) {
            // Unable to load without setting the property
            logger_.warning(
                    "Set the property " + REPORT_FILE + " before loading");
        } else {
            // Trace input
            logger_.fine("Loading from " + reportFile);
            FileInputStream file = null;
            try {
                // The property is set; try to read the file
                file = new FileInputStream(reportFile);
                loadFrom(file);
            } catch (FileNotFoundException | JAXBException e) {
                // The file cannot be loaded
                throw new IOException(e);
            } finally {
                // Always close the file
                if (file != null) {
                    file.close();
                }
            }
        }
    }

    /**
     * Load the report from an XML stream.
     * @param stream
     *            Stream to load the report from
     * @throws JAXBException
     *             Cannot unmarshall the stream as a coverage report
     */
    public void loadFrom(final InputStream stream) throws JAXBException {
        // Use a JAXB context to load the registry
        final JAXBContext jaxbContext = JAXBContext
                .newInstance(XmlCoverageReport.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        // Unmarshall the stream to a new instance, then copy attributes
        final XmlCoverageReport xml = (XmlCoverageReport) jaxbUnmarshaller
                .unmarshal(stream);
        coverage_.putAll(xml.getCoverage());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save() throws IOException {
        // Then store the content of the report to a file, if the report path is
        // defined in the system properties.
        final String reportFile = System.getProperty(REPORT_FILE);
        if (reportFile == null) {
            // Unable to save without setting the property
            logger_.warning(
                    "Set the property " + REPORT_FILE + " before saving");
        } else {
            // Trace output
            logger_.fine("Saving to " + reportFile);
            FileOutputStream file = null;
            try {
                // The property is set; try to write the file
                file = new FileOutputStream(reportFile);
                saveTo(file);
                file.close();
            } catch (FileNotFoundException | JAXBException e) {
                // The file cannot be saved
                throw new IOException(e);
            } finally {
                // Always close the file
                if (file != null) {
                    file.close();
                }
            }
        }
    }

    /**
     * Save the report to an XML stream.
     * @param stream
     *            Stream to save the report to
     * @throws JAXBException
     *             Cannot marshall the content of the report
     */
    public void saveTo(final OutputStream stream) throws JAXBException {
        // Use a JAXB context to save the registry
        final JAXBContext jaxbContext = JAXBContext
                .newInstance(XmlCoverageReport.class);
        final Marshaller jaxbMarhsaller = jaxbContext.createMarshaller();
        // Directly marshall this object to the stream
        jaxbMarhsaller.marshal(this, stream);
    }

    /**
     * Set the list of coverage indexed by requirement version. Should be used
     * only for serialization purpose.
     * @param coverage
     *            Coverage to set
     */
    @XmlElement
    public void setCoverage(final Map<String, CoverageSet> coverage) {
        coverage_ = coverage;
    }

}
