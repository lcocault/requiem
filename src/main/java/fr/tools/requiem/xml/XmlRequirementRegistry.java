/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.tools.requiem.requirement.Requirement;
import fr.tools.requiem.requirement.RequirementRegistry;
import fr.tools.requiem.requirement.RequirementVersion;

/**
 * XML registry of requirements.
 */
@XmlRootElement
public class XmlRequirementRegistry implements RequirementRegistry {

    /** System property to access the requirements file. */
    private static final String REGISTRY_FILE = "fr.tools.requiem.xml.registryPath";

    /** Logger for warnings. */
    private Logger logger_;

    /** Set of requirements. */
    private Set<Requirement> requirements_;

    /**
     * Set of requirements.
     */
    public XmlRequirementRegistry() {
        // Initialize the collection of the registry
        requirements_ = new TreeSet<Requirement>();
        logger_ = Logger.getLogger(getClass().getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(final Requirement requirement) {
        requirements_.add(requirement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Requirement> getRequirements() {
        return requirements_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() throws IOException {
        // Attempt to load an existing version of the registry
        final String registryFile = System.getProperty(REGISTRY_FILE);
        if (registryFile == null) {
            // Unable to load without setting the property
            logger_.warning("Set the property " + REGISTRY_FILE +
                    " before loading registry");
        } else {
            try {
                // The property is set; try to read the file
                loadFrom(new FileInputStream(registryFile));
            } catch (FileNotFoundException | JAXBException e) {
                // The file cannot be loaded
                throw new IOException(e);
            }
        }
    }

    /**
     * Load the registry from an XML stream.
     * @param stream
     *            Stream to load the registry from
     * @throws JAXBException
     *             Cannot unmarshall the stream as a requirement registry
     */
    public void loadFrom(final InputStream stream) throws JAXBException {
        // Use a JAXB context to load the registry
        final JAXBContext jaxbContext = JAXBContext
                .newInstance(XmlRequirementRegistry.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        // Unmarshall the stream to a new instance, then copy attributes by
        // rebuilding the data
        final XmlRequirementRegistry xml = (XmlRequirementRegistry) jaxbUnmarshaller
                .unmarshal(stream);
        for (Requirement xmlRequirement : xml.getRequirements()) {

            // Re-create the requirement
            final Requirement requirement = new Requirement(
                    xmlRequirement.getIdentifier(), xmlRequirement.getType());

            for (RequirementVersion xmlVersion : xmlRequirement.getVersions()) {
                // Re-create each version
                requirement.addVersion(xmlVersion.getVersion(),
                        xmlVersion.getState(), xmlVersion.getText());
            }

            // Add the requirement to the final registry
            requirements_.add(requirement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save() throws IOException {
        // Then store the content of the registry to a file, if the report path
        // is defined in the system properties.
        final String registryFile = System.getProperty(REGISTRY_FILE);
        if (registryFile == null) {
            // Unable to save without setting the property
            logger_.warning("Set the property " + REGISTRY_FILE +
                    " before saving registry");
        } else {
            try {
                // The property is set; try to write the file
                saveTo(new FileOutputStream(registryFile));
            } catch (FileNotFoundException | JAXBException e) {
                // The file cannot be saved
                throw new IOException(e);
            }
        }
    }

    /**
     * Save the registry to an XML stream.
     * @param stream
     *            Stream to save the registry to
     * @throws JAXBException
     *             Cannot marshall the content of the registry
     */
    public void saveTo(final OutputStream stream) throws JAXBException {
        // Use a JAXB context to save the registry
        final JAXBContext jaxbContext = JAXBContext
                .newInstance(XmlRequirementRegistry.class);
        final Marshaller jaxbMarhsaller = jaxbContext.createMarshaller();
        // Directly marshall this object to the stream
        jaxbMarhsaller.marshal(this, stream);
    }

    /**
     * Set the requirements of the registry. Should be used only for
     * serialization purpose.
     * @param requirements
     *            Requirements to set
     */
    @XmlElement
    public void setRequirements(final Set<Requirement> requirements) {
        requirements_ = requirements;
    }

}
