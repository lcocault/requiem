/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.xml;

import java.util.HashSet;

import javax.xml.bind.annotation.XmlElement;

import fr.tools.requiem.coverage.CoverageStatus;
import fr.tools.requiem.coverage.RequirementCoverage;

/**
 * Set of coverage for one requirement version.
 */
class CoverageSet {

    /** Set of coverage. */
    private HashSet<RequirementCoverage> set_;

    /**
     * Constructor.
     */
    public CoverageSet() {
        set_ = new HashSet<RequirementCoverage>();
    }

    /**
     * Add a coverage to the set.
     * @param requirementCoverage
     *            Requirement coverage to add
     */
    public void add(final RequirementCoverage requirementCoverage) {
        set_.add(requirementCoverage);
    }

    /**
     * Get the set of coverage.
     * @return Coverage set
     */
    public HashSet<RequirementCoverage> getSet() {
        return set_;
    }

    /**
     * Get a synthetic status of the set.
     * @return The status of the set
     */
    public CoverageStatus getStatus() {

        // Initial value of the status
        CoverageStatus status = CoverageStatus.COVERED;

        if (size() == 0) {
            // Without any status, the global status is not covered
            status = CoverageStatus.NOT_COVERED;
        }

        for (RequirementCoverage coverage : set_) {
            // If at least one coverage status is not covered, the global status
            // is not covered.
            if (coverage.getStatus() == CoverageStatus.NOT_COVERED) {
                status = CoverageStatus.NOT_COVERED;
            }
        }

        return status;
    }

    /**
     * Set the set of coverage.
     * @param set
     *            Coverage set
     */
    @XmlElement
    public void setSet(final HashSet<RequirementCoverage> set) {
        set_ = set;
    }

    /**
     * Get the number of coverage statuses in the set.
     * @return Size of the set
     */
    public int size() {
        return set_.size();
    }

}
