/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.maven;

import java.io.File;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Maven plugin to render a requirement coverage report.
 */
@Mojo(name = "requiem")
public class RequiemCoverageMavenPlugin extends AbstractMojo {

    /** HTML report path. */
    @Parameter
    private String htmlReport;

    /** XSL path. */
    @Parameter
    private String stylesheet;

    /** XML report path. */
    @Parameter
    private String xmlReport;

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        if (xmlReport == null) {

            // Unable to process the report without setting the property
            throw new MojoExecutionException(
                    "Set the XML report property before loading");

        } else if (htmlReport == null) {

            // Unable to process the report without setting the property
            throw new MojoExecutionException(
                    "Set the HTML report property before loading");

        } else if (stylesheet == null) {

            // Unable to process the report without setting the property
            throw new MojoExecutionException(
                    "Set the stylesheet property before loading");

        } else {

            // Reads data using the Source interface
            final Source xmlSrc = new StreamSource(new File(xmlReport));
            final Source xsltSrc = new StreamSource(stylesheet);

            try {

                // Transform the XML report into an HTML version
                final TransformerFactory factory = TransformerFactory
                        .newInstance();
                final Transformer transformation = factory
                        .newTransformer(xsltSrc);
                transformation.transform(xmlSrc, new StreamResult(htmlReport));

            } catch (TransformerException exception) {

                // Throw a Maven plugin exception
                throw new MojoExecutionException("Cannot transform " +
                        xmlReport + " into " + htmlReport);
            }
        }
    }

    /**
     * Set the HTML report path.
     * @param htmlReport
     *            Path of the HTML report
     */
    public void setHtmlReport(final String htmlReport) {
        this.htmlReport = htmlReport;
    }

    /**
     * Set the XSL stylesheet path.
     * @param stylesheet
     *            Path of the stylesheet
     */
    public void setStylesheet(final String stylesheet) {
        this.stylesheet = stylesheet;
    }

    /**
     * Set the XML report path.
     * @param xmlReport
     *            Path of the XML report
     */
    public void setXmlReport(final String xmlReport) {
        this.xmlReport = xmlReport;
    }
}
