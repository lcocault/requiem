/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

/**
 * Type of requirement.
 */
public enum RequirementType {
    /** Type of requirement that specifies a specific design or architecture. */
    DESIGN,
    /** Type of requirement that specifies human related factors. */
    ERGONOMICS,
    /** Type of requirement that specifies a feature. */
    FUNCTIONAL,
    /** Type of requirement that specifies an interface between subsystems. */
    INTERFACE,
    /** Type of requirement that specifies maintainability level. */
    MAINTAINABILITY,
    /** Type of requirement that specifies a performance expectations. */
    PERFORMANCE,
    /** Type of requirement that specifies portability constraints. */
    PORTABILITY,
    /** Type of requirement that specifies quality rules. */
    QUALITY,
    /** Type of requirement that specifies reliability expectations. */
    RELIABILITY,
    /** Type of requirement that specifies a resource consumption limitation. */
    RESOURCES,
    /** Type of requirement that specifies safety constraints. */
    SAFETY,
    /** Type of requirement that specifies a security or privacy constraint. */
    SECURITY
}
