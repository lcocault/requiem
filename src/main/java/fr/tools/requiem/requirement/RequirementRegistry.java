/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

import java.io.IOException;
import java.util.Set;

/**
 * Specification of a requirements registry.
 */
public interface RequirementRegistry {

    /**
     * Add a new requirement to the registry.
     * @param requirement
     *            Requirement to add to the registry
     */
    void add(Requirement requirement);

    /**
     * Get the requirements of the registry.
     * @return Registry requirements
     */
    Set<Requirement> getRequirements();

    /**
     * Load the registry.
     * @throws IOException
     *             Cannot load the registry from its physical support
     */
    void load() throws IOException;

    /**
     * Save the registry.
     * @throws IOException
     *             Cannot save the registry to its physical support
     */
    void save() throws IOException;

}
