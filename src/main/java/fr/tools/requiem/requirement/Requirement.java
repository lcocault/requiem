/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Definition of a requirement.
 */
public class Requirement implements Comparable<Requirement> {

    /** Identifier of the requirement. */
    private String identifier_;

    /** Type of the requirement. */
    private RequirementType type_;

    /** Successive versions of the requirement. */
    private SortedSet<RequirementVersion> versions_;

    /**
     * Default constructor. Should be used for serialization purpose only.
     */
    @Deprecated
    public Requirement() {
        identifier_ = "Undefined";
        type_ = RequirementType.FUNCTIONAL;
    }

    /**
     * Constructor.
     * @param identifier
     *            Identifier of the requirement
     * @param type
     *            Type of the requirement
     */
    public Requirement(final String identifier, final RequirementType type) {
        identifier_ = identifier;
        type_ = type;
        versions_ = new TreeSet<RequirementVersion>();
    }

    /**
     * Add a version to the requirement.
     * @param version
     *            Version to add
     * @param state
     *            State of the added version
     * @param text
     *            Text of the version
     * @return The added requirement version
     */
    public RequirementVersion addVersion(final short version,
            final RequirementState state, final String text) {
        // Create a requirement version...
        final RequirementVersion requirementVersion = new RequirementVersion(
                identifier_, version, state, text);
        // ...and add it to the requirement
        versions_.add(requirementVersion);
        return requirementVersion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final Requirement other) {
        return identifier_.compareTo(other.identifier_);
    }

    /**
     * Get the identifier of the requirement.
     * @return Requirement identifier
     */
    public String getIdentifier() {
        return identifier_;
    }

    /**
     * Get the type of the requirement.
     * @return Requirement type
     */
    public RequirementType getType() {
        return type_;
    }

    /**
     * Get the versions of the requirement.
     * @return Requirement versions
     */
    public SortedSet<RequirementVersion> getVersions() {
        return versions_;
    }

    /**
     * Set the identifier of the requirement.
     * @param identifier
     *            Requirement identifier
     */
    @Deprecated
    public void setIdentifier(final String identifier) {
        identifier_ = identifier;
    }

    /**
     * Set the type of the requirement.
     * @param type
     *            Requirement type
     */
    @Deprecated
    public void setType(final RequirementType type) {
        type_ = type;
    }

    /**
     * Set the versions of the requirement.
     * @param versions
     *            Requirement versions
     */
    @Deprecated
    public void setVersions(final SortedSet<RequirementVersion> versions) {
        versions_ = versions;
    }

}
