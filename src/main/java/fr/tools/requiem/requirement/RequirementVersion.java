/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

/**
 * Version of a requirement.
 */
public class RequirementVersion implements Comparable<RequirementVersion> {

    /** Identifier of the requirement. */
    private String requirement_;

    /** Version of the requirement. */
    private short version_;

    /** State of the requirement. */
    private RequirementState state_;

    /** Content of the requirement version. */
    private String text_;

    /**
     * Default constructor. Should be used only for serialization purpose.
     */
    @Deprecated
    public RequirementVersion() {
        requirement_ = "Undefined";
        version_ = (short) 0;
        state_ = RequirementState.PROPOSED;
        text_ = "Undefined";
    }

    /**
     * Constructor of a requirement version.
     * @param requirement
     *            Identifier of the requirement
     * @param version
     *            Version of the requirement
     * @param state
     *            State of the requirement
     * @param text
     *            Text of the requirement version
     */
    protected RequirementVersion(final String requirement, final short version,
            final RequirementState state, final String text) {
        requirement_ = requirement;
        version_ = version;
        state_ = state;
        text_ = text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final RequirementVersion other) {
        if (requirement_.equals(other.requirement_)) {
            return version_ - other.version_;
        } else {
            return requirement_.compareTo(other.requirement_);
        }
    }

    /**
     * Get the identifier of the requirement.
     * @return Requirement identifier
     */
    public String getRequirement() {
        return requirement_;
    }

    /**
     * Get the state of the requirement.
     * @return Requirement state
     */
    public RequirementState getState() {
        return state_;
    }

    /**
     * Get the content of the requirement.
     * @return Requirement content
     */
    public String getText() {
        return text_;
    }

    /**
     * Get the version of the requirement.
     * @return Requirement version
     */
    public short getVersion() {
        return version_;
    }

    /**
     * Set the identifier of the requirement.
     * @param requirement
     *            Requirement identifier
     */
    @Deprecated
    public void setRequirement(final String requirement) {
        requirement_ = requirement;
    }

    /**
     * Set the state of the requirement.
     * @param state
     *            Requirement state
     */
    @Deprecated
    public void setState(final RequirementState state) {
        state_ = state;
    }

    /**
     * Set the text of the requirement.
     * @param text
     *            Requirement text
     */
    @Deprecated
    public void setText(final String text) {
        text_ = text;
    }

    /**
     * Set the version of the requirement.
     * @param version
     *            Requirement version
     */
    @Deprecated
    public void setVersion(final short version) {
        version_ = version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return requirement_ + " v" + version_;
    }
}
