/* Copyright 2020 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.StringTokenizer;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import fr.tools.requiem.coverage.CoverageReport;
import fr.tools.requiem.coverage.CoverageStatus;

/**
 * JUnit extension to automatically evaluate the coverage status of requirements
 * attached to the test methods.
 */
public class CoverageUpdater implements AfterTestExecutionCallback {

    /** Coverage report to update. */
    private static CoverageReport report;

    /**
     * Change the coverage report associated with the updater.
     * @param coverageReport
     *            Coverage report to update
     */
    public static void setReport(CoverageReport coverageReport) {
        report = coverageReport;
    }

    /**
     * Get the current coverage report.
     * @return Updated report
     */
    public static CoverageReport getReport() {
        return report;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        // Get the test method and test result from the context
        final Method method = context.getTestMethod().get();
        final boolean hashFailed = context.getExecutionException().isPresent();

        // We only process the Cover annotation that specifies the covered
        // requirement
        for (Annotation annotation : method.getAnnotations()) {
            if (annotation instanceof Cover) {
                String[] requirements = ((Cover) annotation).requirements();
                for (String req : requirements) {
                    StringTokenizer tokenizer = new StringTokenizer(req);
                    String requirement = tokenizer.nextToken();
                    String version = tokenizer.nextToken();
                    CoverageStatus status = hashFailed
                            ? CoverageStatus.NOT_COVERED
                            : CoverageStatus.COVERED;

                    // Register the status
                    if (report != null) {
                        report.add(requirement, version, status);
                    }
                }
            }
        }
    }
}
