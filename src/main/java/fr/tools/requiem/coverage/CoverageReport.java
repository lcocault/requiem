/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.coverage;

import java.io.IOException;
import java.util.Map;

import fr.tools.requiem.requirement.RequirementRegistry;

/**
 * Coverage report interface.
 */
public interface CoverageReport {

    /**
     * Add a requirement coverage to the report.
     * @param requirement
     *            Requirement covered
     * @param version
     *            Version of the covered requirement
     * @param status
     *            Coverage status
     */
    void add(String requirement, String version, CoverageStatus status);

    /**
     * Get the known requirements and the corresponding status.
     * @return Coverage status indexed by the covered requirement
     */
    Map<String, CoverageStatus> getStatuses();

    /**
     * Get the coverage of a requirement.
     * @param requirement
     *            Requirement for which the coverage is required
     * @return Coverage status of the requirement, null if the requirement is
     *         not in the database
     */
    CoverageStatus getStatus(String requirement);

    /**
     * Get the coverage of a requirement.
     * @param requirement
     *            Requirement for which the coverage is required
     * @param version
     *            Version of the requirement for which the coverage is required
     * @return Coverage status of the requirement/version, null if the
     *         requirement is not in the database
     */
    CoverageStatus getStatus(String string, String string2);

    /**
     * Initialize the coverage report with a registry of requirements.
     * @param registry
     *            Requirements registry
     */
    void initialize(RequirementRegistry registry);

    /**
     * Load the report.
     * @throws IOException
     *             Cannot load the report from its physical support
     */
    void load() throws IOException;

    /**
     * Save the report.
     * @throws IOException
     *             Cannot save the report to its physical support
     */
    void save() throws IOException;
}
