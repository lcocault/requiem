/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.coverage;

import java.util.Date;

/**
 * Requirement coverage.
 */
public class RequirementCoverage {

    /** Covered requirement. */
    private String requirement_;

    /** Coverage status. */
    private CoverageStatus status_;

    /** Date of coverage. */
    private Date date_;

    /** Testing element. */
    private String test_;

    /**
     * Default constructor. Should be used only for serialization purpose.
     */
    @Deprecated
    public RequirementCoverage() {
        requirement_ = "Undefined";
        status_ = CoverageStatus.NOT_COVERED;
        date_ = null;
        test_ = "Undefined";
    }

    /**
     * Constructor.
     * @param requirement
     *            Requirement the coverage is related to
     * @param status
     *            Status of coverage
     */
    public RequirementCoverage(final String requirement,
            final CoverageStatus status) {
        // Direct assignment
        requirement_ = requirement;
        status_ = status;
        // Computed fields
        final StackTraceElement[] stack = Thread.currentThread()
                .getStackTrace();
        int index = 0;
        while (index < stack.length && test_ == null) {
            // FIXME Make the constant a configuration parameter ?
            if (stack[index].getClassName().matches(".*Test.*")) {
                final StackTraceElement elt = stack[index];
                test_ = elt.getClassName() + ":" + elt.getLineNumber();
            }
            index++;
        }
        date_ = new Date();
    }

    /**
     * Get the date of coverage.
     * @return Coverage date
     */
    public Date getDate() {
        return date_;
    }

    /**
     * Get the covered requirement.
     * @return Requirement covered (or not)
     */
    public String getRequirement() {
        return requirement_;
    }

    /**
     * Get the coverage status.
     * @return Status of coverage
     */
    public CoverageStatus getStatus() {
        return status_;
    }

    /**
     * Get the testing element.
     * @return Code element testing the coverage
     */
    public String getTest() {
        return test_;
    }

    /**
     * Set the date of coverage.
     * @param date
     *            Coverage date
     */
    @Deprecated
    public void setDate(final Date date) {
        date_ = date;
    }

    /**
     * Set the covered requirement.
     * @param requirement
     *            Requirement covered (or not)
     */
    @Deprecated
    public void setRequirement(final String requirement) {
        requirement_ = requirement;
    }

    /**
     * Set the coverage status.
     * @param status
     *            Status of coverage
     */
    @Deprecated
    public void setStatus(final CoverageStatus status) {
        status_ = status;
    }

    /**
     * Set the testing element.
     * @param test
     *            Code element testing the coverage
     */
    @Deprecated
    public void setTest(final String test) {
        test_ = test;
    }

}
