<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<!-- HTML target -->
	<xsl:output method="html" />

	<!-- BODY -->
	<xsl:template match="/">
		<HTML>
			<BODY>
				<TABLE>
					<TR>
                        <TH>Requirement</TH>
                        <TH>Status</TH>
					</TR>
					<xsl:apply-templates />
				</TABLE>
			</BODY>
		</HTML>
	</xsl:template>

	<!-- Requirement -->
	<xsl:template match="entry">
		<TR>
			<TD><xsl:value-of select="key" /></TD>
			<xsl:apply-templates select="value"/>
		</TR>
	</xsl:template>

	<!-- Coverage -->
	<xsl:template match="value">
		<TD>
			<xsl:for-each select="set">
                <xsl:if test="status='COVERED'"><IMG src="classes/ok.png"/></xsl:if>
                <xsl:if test="status='NOT_COVERED'"><IMG src="classes/ko.png"/></xsl:if>
			</xsl:for-each>
		</TD>
	</xsl:template>

</xsl:stylesheet>
