/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test the class Requirement.
 */
public class TestRequirement {

    @Test
    public void testBasic() {

        // Constructor
        Requirement requirement = new Requirement("Identifier",
                RequirementType.FUNCTIONAL);

        // Getters
        assertEquals("Identifier", requirement.getIdentifier());
        assertEquals(RequirementType.FUNCTIONAL, requirement.getType());
        assertEquals(0, requirement.getVersions().size());

        // Add versions
        RequirementVersion v1 = requirement.addVersion((short) 1,
                RequirementState.OBSOLETE, "1st version");
        RequirementVersion v2 = requirement.addVersion((short) 2,
                RequirementState.APPROVED, "2nd version");
        RequirementVersion v3 = requirement.addVersion((short) 3,
                RequirementState.PROPOSED, "3rd version");
        assertEquals(3, requirement.getVersions().size());
        assertEquals(v1, requirement.getVersions().first());
        assertEquals(v3, requirement.getVersions().last());
        assertEquals(requirement.getIdentifier(), v2.getRequirement());
    }

    @Test
    public void testComparison() {

        // Constructor
        Requirement req = new Requirement("ID-002", RequirementType.FUNCTIONAL);
        Requirement bef = new Requirement("ID-001", RequirementType.FUNCTIONAL);
        Requirement aft = new Requirement("ID-003", RequirementType.FUNCTIONAL);
        Requirement equ = new Requirement("ID-002", RequirementType.FUNCTIONAL);

        // Getters
        assertTrue(req.compareTo(aft) < 0);
        assertTrue(req.compareTo(bef) > 0);
        assertTrue(req.compareTo(equ) == 0);
    }
}
