/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.requirement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test the class RequirementVersion.
 */
public class TestRequirementVersion {

    @Test
    public void testBasic() {

        // Constructor
        RequirementVersion version = new RequirementVersion("ID-003", (short) 7,
                RequirementState.PROPOSED, "Text");

        // Getters
        assertEquals("ID-003", version.getRequirement());
        assertEquals("Text", version.getText());
        assertEquals(RequirementState.PROPOSED, version.getState());
        assertEquals((short) 7, version.getVersion());

        // String-ification
        assertEquals("ID-003 v7", version.toString());
    }

    @Test
    public void testComparison() {

        // Constructor
        RequirementVersion version = new RequirementVersion("ID-007", (short) 7,
                RequirementState.PROPOSED, "Text");
        RequirementVersion recent = new RequirementVersion("ID-007", (short) 8,
                RequirementState.PROPOSED, "Text");
        RequirementVersion older = new RequirementVersion("ID-007", (short) 6,
                RequirementState.PROPOSED, "Text");
        RequirementVersion equal = new RequirementVersion("ID-007", (short) 7,
                RequirementState.PROPOSED, "Text");
        RequirementVersion otherRequirement = new RequirementVersion("ID-003",
                (short) 7, RequirementState.PROPOSED, "Text");

        // Getters
        assertTrue(version.compareTo(recent) < 0);
        assertTrue(version.compareTo(older) > 0);
        assertTrue(version.compareTo(equal) == 0);
        assertTrue(version.compareTo(otherRequirement) > 0);
    }

}
