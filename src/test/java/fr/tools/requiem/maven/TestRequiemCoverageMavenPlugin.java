/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.maven;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.junit.jupiter.api.Test;

/**
 * Test the XML coverage report.
 */
public class TestRequiemCoverageMavenPlugin {

    @Test
    public void testSuccessfulGeneration() {

        try {
            // Deposit a coverage file
            Path srcReport = FileSystems.getDefault()
                    .getPath("src/test/resources", "report.xml");
            Files.copy(srcReport, new FileOutputStream("target/report.xml"));
        } catch (IOException e) {
            // Useless to go further
            fail("Cannot initialize the context : " + e.getMessage());
        }

        // Create the plugin and execute it
        RequiemCoverageMavenPlugin plugin = new RequiemCoverageMavenPlugin();
        plugin.setStylesheet("src/main/resources/htmlreport.xsl");
        plugin.setXmlReport("target/report.xml");
        plugin.setHtmlReport("target/report.html");

        try {
            plugin.execute();
        } catch (Exception e) {
            // Execution should be nominal
            e.printStackTrace();
            fail();
        }

        // Check the existence of the HTML report
        assertTrue(new File("target/report.html").exists());
    }

    @Test
    public void testGenerationFailures() {

        // Plugin to test
        RequiemCoverageMavenPlugin plugin = new RequiemCoverageMavenPlugin();

        // Initialize only the target reference and fail to execute
        plugin.setHtmlReport("target/report.html");
        plugin.setStylesheet("src/main/resources/htmlreport.xsl");
        try {
            plugin.execute();
            fail("Input file reference is mandatory");
        } catch (MojoExecutionException e) {
            // Nominal path
            System.out.println(e.getMessage());
        } catch (MojoFailureException e) {
            // Wrong path
            e.printStackTrace();
            fail("Missing input should fail with execution exception");
        }

        // Initialize only the source reference and fail to execute
        plugin.setStylesheet("src/main/resources/htmlreport.xsl");
        plugin.setXmlReport("target/report.xml");
        plugin.setHtmlReport(null);
        try {
            plugin.execute();
            fail("Output file reference is mandatory");
        } catch (MojoExecutionException e) {
            // Nominal path
            System.out.println(e.getMessage());
        } catch (MojoFailureException e) {
            // Wrong path
            e.printStackTrace();
            fail("Missing output should fail with execution exception");
        }

        // Miss initializing the stylesheet and fail to execute
        plugin.setStylesheet(null);
        plugin.setXmlReport("target/report.xml");
        plugin.setHtmlReport("target/report.html");
        try {
            plugin.execute();
            fail("Stylesheet reference is mandatory");
        } catch (MojoExecutionException e) {
            // Nominal path
            System.out.println(e.getMessage());
        } catch (MojoFailureException e) {
            // Wrong path
            e.printStackTrace();
            fail("Missing stylesheet should fail with execution exception");
        }

        // Initialize an invalid target reference and fail to execute
        plugin.setStylesheet("src/main/resources/htmlreport.xsl");
        plugin.setHtmlReport("unknown/report.html");
        try {
            plugin.execute();
            fail("Output file should be accessible");
        } catch (MojoExecutionException e) {
            // Nominal path
            System.out.println(e.getMessage());
        } catch (MojoFailureException e) {
            // Wrong path
            e.printStackTrace();
            fail("Invalid output should fail with execution exception");
        }
    }
}
