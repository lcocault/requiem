package fr.tools.requiem.annotation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.tools.requiem.coverage.CoverageReport;
import fr.tools.requiem.coverage.CoverageStatus;
import fr.tools.requiem.requirement.RequirementRegistry;

public class TestCoverData implements CoverageReport {

    /** Format of the key. */
    private static final String KEY_FORMAT = "%s %s";

    /** Declared status. */
    public Map<String, CoverageStatus> statuses = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(String requirement, String version, CoverageStatus status) {
        String key = String.format(KEY_FORMAT, requirement, version);
        statuses.put(key, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, CoverageStatus> getStatuses() {
        return statuses;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CoverageStatus getStatus(String requirement) {
        throw new RuntimeException("Not implemented");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CoverageStatus getStatus(String requirement, String version) {
        String key = String.format(KEY_FORMAT, requirement, version);
        return statuses.get(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(RequirementRegistry registry) {
        // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() throws IOException {
        // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save() throws IOException {
        // Not implemented
    }

}