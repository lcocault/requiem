/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;

import fr.tools.requiem.requirement.Requirement;
import fr.tools.requiem.requirement.RequirementRegistry;
import fr.tools.requiem.requirement.RequirementState;
import fr.tools.requiem.requirement.RequirementType;

/**
 * Test the XML requirement registry.
 */
public class TestXmlRequirementRegistry {

    @Test
    public void testXmlSerialization() {

        // Fixture
        XmlRequirementRegistry registry = new XmlRequirementRegistry();
        Requirement req1 = new Requirement("ID-01", RequirementType.FUNCTIONAL);
        Requirement req2 = new Requirement("ID-02",
                RequirementType.PERFORMANCE);
        req1.addVersion((short) 1, RequirementState.APPROVED, "Text F1");
        req1.addVersion((short) 2, RequirementState.PROPOSED, "Text F2");
        req2.addVersion((short) 1, RequirementState.OBSOLETE, "Text P1");
        req2.addVersion((short) 2, RequirementState.APPROVED, "Text P2");
        registry.add(req1);
        registry.add(req2);

        try {

            // Save the registry to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            registry.saveTo(bos);

            // Reload it in a new registry
            XmlRequirementRegistry copy = new XmlRequirementRegistry();
            ByteArrayInputStream bis = new ByteArrayInputStream(
                    bos.toByteArray());
            copy.loadFrom(bis);

            // Check the parsed registry
            Iterator<Requirement> reqs = copy.getRequirements().iterator();
            for (int i = 0; i < 2; i++) {

                // Get the next requirement
                assertTrue(reqs.hasNext());
                Requirement req = reqs.next();

                if (req.getIdentifier().equals("ID-01")) {
                    // Requirement 1
                    assertEquals(RequirementType.FUNCTIONAL, req.getType());
                    assertEquals(2, req.getVersions().size());
                    assertEquals("Text F1",
                            req.getVersions().first().getText());
                    assertEquals(RequirementState.APPROVED,
                            req.getVersions().first().getState());
                    assertEquals("Text F2", req.getVersions().last().getText());
                    assertEquals(RequirementState.PROPOSED,
                            req.getVersions().last().getState());
                } else if (req.getIdentifier().equals("ID-02")) {
                    // Requirement 2
                    assertEquals(RequirementType.PERFORMANCE, req.getType());
                    assertEquals(2, req.getVersions().size());
                    assertEquals("Text P1",
                            req.getVersions().first().getText());
                    assertEquals(RequirementState.OBSOLETE,
                            req.getVersions().first().getState());
                    assertEquals("Text P2", req.getVersions().last().getText());
                    assertEquals(RequirementState.APPROVED,
                            req.getVersions().last().getState());
                } else {
                    fail("Unexpected requirement : " + req.getIdentifier());
                }
            }

        } catch (JAXBException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testRegistryInitialization() {

        // The registry does not initially exist
        Path targetRegistry = FileSystems.getDefault().getPath("target",
                "registry.xml");
        Path savedRegistry = FileSystems.getDefault().getPath("target",
                "saved.xml");
        try {
            Files.deleteIfExists(targetRegistry);
            Files.deleteIfExists(savedRegistry);
        } catch (IOException e) {
            // Useless to go further
            fail("Cannot initialize the context : " + e.getMessage());
        }

        // Create a registry without loading
        RequirementRegistry registry = new XmlRequirementRegistry();
        try {
            registry.load();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertTrue(registry.getRequirements().isEmpty());

        // Attempt to save with specifying the system property
        try {
            registry.save();
        } catch (IOException e) {
            fail(e.getMessage());
        }

        // Create a report with a wrong reference
        System.setProperty("fr.tools.requiem.xml.registryPath",
                "unknown/registry.xml");
        try {
            registry.load();
            fail("Should not be loadable with an invalid registry path");
        } catch (IOException e) {
            // Nominal path
        }
        assertTrue(registry.getRequirements().isEmpty());

        // Attempt to save with a wrong system property
        try {
            registry.save();
            fail("Should not be saved with an invalid registry path");
        } catch (IOException e) {
            // Nominal path
        }

        // Create a report with correct loading
        System.setProperty("fr.tools.requiem.xml.registryPath",
                "target/registry.xml");
        try {
            // Deposit a registry file
            Path srcRegistry = FileSystems.getDefault()
                    .getPath("src/test/resources", "registry.xml");
            Files.copy(srcRegistry,
                    new FileOutputStream("target/registry.xml"));
        } catch (IOException e) {
            // Useless to go further
            fail("Cannot initialize the context : " + e.getMessage());
        }
        try {
            registry.load();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertFalse(registry.getRequirements().isEmpty());

        // Successful saving
        System.setProperty("fr.tools.requiem.xml.registryPath",
                "target/saved.xml");
        try {
            registry.save();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertTrue(Files.exists(savedRegistry));

        // Clear the property for other tests
        System.clearProperty("fr.tools.requiem.xml.registryPath");
    }
}
