/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.requiem.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;

import fr.tools.requiem.coverage.CoverageReport;
import fr.tools.requiem.coverage.CoverageStatus;
import fr.tools.requiem.requirement.Requirement;
import fr.tools.requiem.requirement.RequirementState;
import fr.tools.requiem.requirement.RequirementType;

/**
 * Test the XML coverage report.
 */
public class TestXmlCoverageReport {

    @Test
    public void testXmlSerialization() {

        // Fixture
        XmlRequirementRegistry registry = new XmlRequirementRegistry();
        XmlCoverageReport report = new XmlCoverageReport();
        Requirement req1 = new Requirement("ID-01", RequirementType.FUNCTIONAL);
        Requirement req2 = new Requirement("ID-02",
                RequirementType.PERFORMANCE);
        Requirement req3 = new Requirement("ID-03",
                RequirementType.RELIABILITY);
        req1.addVersion((short) 1, RequirementState.APPROVED, "Text F1");
        req1.addVersion((short) 2, RequirementState.PROPOSED, "Text F2");
        req2.addVersion((short) 1, RequirementState.OBSOLETE, "Text P1");
        req2.addVersion((short) 2, RequirementState.APPROVED, "Text P2");
        req3.addVersion((short) 1, RequirementState.APPROVED, "Text R1");
        registry.add(req1);
        registry.add(req2);
        registry.add(req3);

        try {

            // Initialize the report with the registry
            report.initialize(registry);

            // Add coverage
            report.add("ID-01", "v1", CoverageStatus.COVERED);
            report.add("ID-01", "v2", CoverageStatus.COVERED);
            report.add("ID-02", "v2", CoverageStatus.NOT_COVERED);
            report.add("ID-02", "v2", CoverageStatus.COVERED);

            // Save the registry to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            report.saveTo(bos);

            // Reload it in a new report
            XmlCoverageReport copy = new XmlCoverageReport();
            ByteArrayInputStream bis = new ByteArrayInputStream(
                    bos.toByteArray());
            copy.loadFrom(bis);

            // Check the parsed report
            assertEquals(3, copy.getCoverage().entrySet().size());
            assertTrue(copy.getCoverage().containsKey("ID-01 v1"));
            assertTrue(copy.getCoverage().containsKey("ID-02 v2"));
            assertTrue(copy.getCoverage().containsKey("ID-03 v1"));
            CoverageSet c1 = copy.getCoverage().get("ID-01 v1");
            CoverageSet c2 = copy.getCoverage().get("ID-02 v2");
            CoverageSet c3 = copy.getCoverage().get("ID-03 v1");
            assertEquals(1, c1.size());
            assertEquals(2, c2.size());
            assertEquals(0, c3.size());
            assertEquals(CoverageStatus.COVERED, c1.getStatus());
            assertEquals(CoverageStatus.NOT_COVERED, c2.getStatus());
            assertEquals(CoverageStatus.NOT_COVERED, c3.getStatus());
            assertEquals(CoverageStatus.COVERED, copy.getStatus("ID-01 v1"));
            assertEquals(CoverageStatus.NOT_COVERED,
                    copy.getStatus("ID-02 v2"));
            assertEquals(CoverageStatus.NOT_COVERED,
                    copy.getStatus("ID-03 v1"));
            assertNull(copy.getStatus("ID-04 v1"));
            assertEquals(3, copy.getStatuses().entrySet().size());
            assertTrue(copy.getStatuses().containsKey("ID-01 v1"));
            assertTrue(copy.getStatuses().containsKey("ID-02 v2"));
            assertTrue(copy.getStatuses().containsKey("ID-03 v1"));
            assertEquals(CoverageStatus.COVERED,
                    copy.getStatuses().get("ID-01 v1"));
            assertEquals(CoverageStatus.NOT_COVERED,
                    copy.getStatuses().get("ID-02 v2"));
            assertEquals(CoverageStatus.NOT_COVERED,
                    copy.getStatuses().get("ID-03 v1"));

        } catch (JAXBException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testReportInitialization() {

        // The report does not initially exist
        Path targetReport = FileSystems.getDefault().getPath("target",
                "report.xml");
        Path savedReport = FileSystems.getDefault().getPath("target",
                "saved.xml");
        try {
            Files.deleteIfExists(targetReport);
            Files.deleteIfExists(savedReport);
        } catch (IOException e) {
            // Useless to go further
            fail("Cannot initialize the context : " + e.getMessage());
        }

        // Create a report without loading
        CoverageReport report = new XmlCoverageReport();
        try {
            report.load();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertTrue(report.getStatuses().isEmpty());

        // Attempt to save with specifying the system property
        try {
            report.save();
        } catch (IOException e) {
            fail(e.getMessage());
        }

        // Create a report with a wrong reference
        System.setProperty("fr.tools.requiem.xml.reportPath",
                "unknown/report.xml");
        try {
            report.load();
            fail("Should not be loadable with an invalid report path");
        } catch (IOException e) {
            // Nominal path
        }
        assertTrue(report.getStatuses().isEmpty());

        // Attempt to save with a wrong system property
        try {
            report.save();
            fail("Should not be saved with an invalid report path");
        } catch (IOException e) {
            // Nominal path
        }

        // Create a report with correct loading
        System.setProperty("fr.tools.requiem.xml.reportPath",
                "target/report.xml");
        try {
            // Deposit a coverage file
            Path srcReport = FileSystems.getDefault()
                    .getPath("src/test/resources", "report.xml");
            Files.copy(srcReport, new FileOutputStream("target/report.xml"));
        } catch (IOException e) {
            // Useless to go further
            fail("Cannot initialize the context : " + e.getMessage());
        }
        try {
            report.load();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertFalse(report.getStatuses().isEmpty());

        // Successful saving
        System.setProperty("fr.tools.requiem.xml.reportPath",
                "target/saved.xml");
        try {
            report.save();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertTrue(Files.exists(savedReport));

        // Clear the property for other tests
        System.clearProperty("fr.tools.requiem.xml.reportPath");
    }
}
